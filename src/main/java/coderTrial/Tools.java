package coderTrial;

import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;

public class Tools {
    public boolean isValidCrop(Block block) { //checks if crop is valid
        MaterialData m = block.getState().getData();
        if(m instanceof Crops) { //checks if m is a crop
          return (((Crops) m).getState() == CropState.RIPE); //returns state of crop
        }
        else {
        	return false;
        }
   }
    public int getDropAmount(ItemStack itemInHand) { //calculates drop off of hoe & fortune
    	int ret = itemInHand.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS); //gets fortune level return
		if (itemInHand != null && itemInHand.getType() == Material.DIAMOND_HOE) { //returns based off of hoe used
			ret+=3;
		} else if (itemInHand != null && itemInHand.getType() == Material.GOLD_HOE || itemInHand.getType() == Material.IRON_HOE) {
			ret+=2;
		} else if (itemInHand != null && itemInHand.getType() == Material.STONE_HOE || itemInHand.getType() == Material.WOOD_HOE) {
			ret+=1;
		} else {
			ret+=0;
		}
		return ret;
    }
    public short getDuraDamage(ItemStack itemInHand) {
		short ret = 0;
		int rnd = (int)(Math.random() * 100); //percent probability
		//(100/(Level+1))%
		if (itemInHand.getEnchantmentLevel(Enchantment.DURABILITY) == 1 && rnd < 50) {
			ret = 1;
		} else if (itemInHand.getEnchantmentLevel(Enchantment.DURABILITY) == 2 && rnd < 33){
			ret = 1;
		} else if (itemInHand.getEnchantmentLevel(Enchantment.DURABILITY) == 3 && rnd < 25) {
			ret = 1;
		} else if (itemInHand.getEnchantmentLevel(Enchantment.DURABILITY) == 0) {
			ret = 1;
		}
		
		return ret;
    }
}
